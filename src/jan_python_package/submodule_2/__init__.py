# submodule init file

def _cli():
    """
    This is the function in submodule 2!

    Returns
    -------

    """
    print("Hello from 2 ", __file__)
    print(f"sub2 {sub_fun(33)}")


def sub_fun(input_arg):
    """
    This is sub fun.

    Parameters
    ----------
    input_arg : int
        Super number! huh?

    Returns
    -------
    int
        Another super number
    """

    return 42
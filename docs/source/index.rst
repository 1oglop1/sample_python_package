.. jan python project documentation master file, created by
   sphinx-quickstart on Fri Jun  9 09:09:46 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

!Welcome to jan python project's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   help
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
